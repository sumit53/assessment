import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ListDataService } from '../app/service/list.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'assessment';
  form: FormGroup
  skills: any[];
  developerType: any[];
  skillsSelectedItems: any[];
  industriesSelectedItems: any[];
  dropdownSettings: any = {};
  toggle = true;
  status = 'Dark';
  setDarkClass: boolean = false;
  constructor(
    private listDataService: ListDataService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.skills = this.listDataService.getSkills();
    this.developerType = this.listDataService.getdeveloperType();
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 2,
      allowSearchFilter: false
    };
    this.createForm();
    // $(function () {
    //   $('[data-toggle="tooltip"]').tooltip()
    // })

  }
  //   enableDisableRule(job) {
  //     this.toggle = !this.toggle;
  //     this.status = this.toggle ? 'Enable' : 'Disable';
  // }
  changeColor() {
    // this.toggle = !this.toggle;
    // this.bgColor = '#'+(0x202020).toString(16);
  }
  toggleClass() {
    this.setDarkClass = !this.setDarkClass;
    this.status = this.setDarkClass ? 'Dark' : 'Light';
  }

  createForm() {
    this.form = this.fb.group({
      fname: ['', [Validators.required, Validators.pattern("^[a-z A-Z]*$")]],
      skills: [''],
      type: [''],
      email: ['', [Validators.required, Validators.email, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/)]],
      password: ['', [Validators.required, Validators.minLength(5)]],
    });

  }
  signIn() {
    if (this.form.valid) {
      localStorage.setItem('userDetails', JSON.stringify(this.form.value))
      alert("Successfully SignIn")
    }
    const controls = this.form.controls;
    /** check form */
    if (this.form.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
    }
  }


  // checkStrength() {
  // this.keyValue = key
  // if(this.keyValue.length < 5){
  //   this.week = true
  // }
  // if (key) {
  //   if ((/[!@#$%*]/).test(key)) {
  //     this.strongArr.push("1")
  //     this.passwordArr.push("1")
  //   }
  //   if ((/[0-9]/).test(key)) {
  //     this.modArr.push("2")
  //     this.passwordArr.push("1")
  //   }
  //   if ((/[a-z]/).test(key)) {
  //     this.weekArr.push("3")
  //     this.passwordArr.push("1")
  //   }
  //   if(this.passwordArr.length >=4 && this.strongArr.length >=1 ){
  //     this.passType ="Strong"
  //     console.log(this.passType)
  //   }
  //   if(this.passwordArr.length >= 4 && this.modArr.length >= 1 ){
  //     this.passType ="moderate"
  //     console.log(this.passType)
  //   }
  //   if(this.passwordArr.length >= 4 && this.weekArr.length >=2){
  //     this.passType ="week"
  //     console.log(this.passType)
  //   }
  // }
  // const key = this.form.controls.password.value;
  // var regularExp = /^[a-zA-Z0-9!@#$%^&*]{6,16}$/;
  //   var special = /[!@#$%*]/;
  //   var num = /[0-9]/;
  //   var alpha = /[a-z]/;
  //   if(special.test(key)){
  //     this.strong =true
  //   }
  //   if(num.test(key)){
  //     this.moderate =true
  //   }
  //   if(alpha.test(key)){
  //     this.week =true
  //   }
  //   if(this.strong && this.moderate && this.week){
  //     console.log("strong")
  //   }
  //   if( this.moderate && this.week){
  //     console.log("mod")
  //   }
  //   if(this.week){
  //     console.log("week")
  //   }

  // }
  onStrengthChange(score) {
    console.log('new score', score);
  }
}
