import { Injectable } from '@angular/core';

@Injectable()
export class ListDataService {

  constructor() { }

  skills: any[] = [
    { id: "1", name: "Html"},
    { id: "2", name: "CSS"},
    { id: "3", name: "Bootstrap"},
    { id: "4", name: "JavaScript"},
    { id: "5", name: "Angular"},
    { id: "7", name: "Node"},
    { id: "8", name: "Java"},
    { id: "9", name: "Python"},
    { id: "10", name: "Php"},
  ];

  developerType: any[] = [
    { id: "1", name: "Frontend"},
    { id: "2", name: "Backend"},
    { id: "3", name: "Full Stack"},
  ];

 
  public getSkills(): any[] {
    return this.skills;
  }
  public getdeveloperType(): any[] {
    return this.developerType;
  }

}