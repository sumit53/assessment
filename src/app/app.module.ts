import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import {ListDataService} from '../app/service/list.service';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { StrengthMeterModule } from "ngx-strength-meter";
import {MatTooltipModule} from '@angular/material/tooltip';
import { PasswordStrengthMeterComponent } from './password-strength-meter/password-strength-meter.component';



@NgModule({
  declarations: [
    AppComponent,
    PasswordStrengthMeterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgMultiSelectDropDownModule.forRoot(),
    MatSlideToggleModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    StrengthMeterModule,
    MatTooltipModule
  ],
  exports:[PasswordStrengthMeterComponent],
  entryComponents:[],
  providers: [ListDataService],
  bootstrap: [AppComponent,]
})
export class AppModule { }
